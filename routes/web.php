<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'admin', 'middleware' => 'check_user_role'], function () {
        Route::any('/', 'HomeController@index')->name('home');
        Route::any('/reports', 'HomeController@reports')->name('reports');

        Route::group(['prefix' => 'tasks'], function () {
            Route::get('/', 'TasksController@index')->name('tasks');
            Route::post('/', 'TasksController@createOrUpdate');
            Route::delete('/', 'TasksController@delete');
        });
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'UserController@index')->name('users');
            Route::post('/', 'UserController@create');
            Route::put('/', 'UserController@update');
            Route::delete('/', 'UserController@delete');
        });
    });
    
    Route::get('/', 'FlowsController@index')->name('flows');
    Route::group(['prefix' => 'flows'], function () {
        Route::get('/', 'FlowsController@index')->name('flows');
        Route::post('/', 'FlowsController@createOrUpdate');
        Route::delete('/', 'FlowsController@delete');
    });
});
