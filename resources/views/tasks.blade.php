@extends('layouts.app')

@section('content')

@if(count($tasks) == 0)
<h3 class="center">Não há tarefas cadastradas</h3>
@endif

@foreach ($tasks as $task)
<div id="{{$task->id}}" class="grid-item z-depth-3">
    <div class="placeholder">
        <div class="gallery-curve-wrapper">
            <div class="gallery-header">
                <p><b class="red-text">Tarefa:</b> {{$task->name}}</p>
                <p><b class="red-text">Obrigatória:</b> {{ ($task->required) ? 'SIM' : "NÃO" }} </p>
                <div class="row center">
                    <a class="btn-flat tooltipped" onclick="editTask({{$task}})" data-position='left' data-delay='50' data-tooltip="Editar Tarefa">
                        <i class="material-icons blue-text">
                            edit
                        </i>
                    </a>
                    <a class="btn-flat tooltipped" onclick="askDelete({{$task->id}})" data-position='bottom' data-delay='50' data-tooltip="Deletar Tarefa">
                        <i class="material-icons red-text">
                            clear
                        </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach


<div class="fixed-action-btn">
    <a class="btn-floating btn-large red  btn tooltipped pulse" data-background-color="red lighten-3" data-position="left" data-delay="50" data-tooltip="Criar Tarefa" onclick="openModal()">
        <i class="large material-icons">add</i>
    </a>
</div>


<!-- Modal Structure -->
<div id="modalDelete" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 class="center red-text">Deletar tarefa?</h4>
        <div class="row center">
            <input type="hidden" id="deleteInput">
            <a class="btn-flat tooltipped" onclick="deleteTask()" data-position='left' data-delay='50' data-tooltip="Sim">
                <i class="material-icons blue-text">
                    done
                </i>
            </a>
            <a class="btn-flat tooltipped" onclick="closeModal()" data-position='right' data-delay='50' data-tooltip="Não">
                <i class="material-icons red-text">
                    close
                </i>
            </a>
        </div>
    </div>
</div>


<!-- Modal Structure -->
<div id="modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 id="task" class="center red-text">Nova tarefa</h4>
        <form class="col s12" method="POST" action="{{ URL::route('tasks') }}" id="formTask">
            <div class="row">
                <div class="input-field col s12">
                    <input id="nameTask" name="name" type="text" class="validate">
                    <label for="disabled">Descrição</label>
                </div>
            </div>

            <div class="row">
                <div class="col s12">
                    <input id="required" type="checkbox" name="required" value="1">
                    <label for="required"> Obrigatória </label>
                </div>
            </div>
    </div>

    <div class="modal-footer">
        <button class="modal-action waves-effect waves-green btn-flat " type="submit">Salvar</button>
        </form>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Fechar</a>
    </div>
</div>
</div>

@if( method_exists($tasks,'links') )
<h1 class="center">{{$tasks->links('vendor.pagination.materializecss')}}</h1>
@endif
@endsection
<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/materialize.min.js') }}"></script>

<script>
    function openModal() {
        this.clean();
        $('#modal').modal('open');
    }

    function closeModal() {
        this.clean();
        $('#modalDelete').modal('close');
    }

    function clean() {
        $('#formTask').get(0).setAttribute('method', 'POST');
        $("#idTask").remove();
        $("#nameTask").val('');
        $("#required").prop('checked', false);
    }

    function editTask(task) {
        $("#idTask").append(task['id']);
        $("#task").html("Editar Tarefa");
        $("#nameTask").val(task['name']);
        required = task['required'];
        if (required) {
            $("#required").prop('checked', true);
        } else {
            $("#required").prop('checked', false);
        }
        $("#typeTask").formSelect();
        $('<input>').attr({
            type: 'hidden',
            task_id: 'idTask',
            name: 'id',
            value: task['id']
        }).appendTo('#formTask');
        M.updateTextFields()
        $('#modal').modal('open');
    }

    function askDelete(id) {
        $('#modalDelete').modal('open');
        $("#deleteInput").val(id);
    }

    function deleteTask() {
        let id = $("#deleteInput").val();
        $.ajax({
            type: 'DELETE',
            url: 'tasks',
            data: {
                "id": id
            },
            success: function($data) {
                $("#" + id).remove();
                $('.grid').masonry('reloadItems');;
                $('.grid').masonry({
                    itemSelector: '.grid-item',
                    columnWidth: 50
                });
                M.toast({
                    html: $data
                }, 5000);
                $("#modalDelete").modal("close");
                $("#deleteInput").val('');
            }
        });
    }
</script>