@extends('layouts.app')

@section('content')

@if(count($flows) == 0)
<h3 class="center">Não há flows cadastrados</h3>
@endif

@foreach ($flows as $flow)
<div id="{{$flow->id}}" class="grid-item z-depth-3">
    <div class="placeholder">
        <div class="gallery-curve-wrapper">
            <div class="gallery-header">
                <p><b class="red-text">Usuário:</b> {{$flow->user->name}}</p>
                <p><b class="red-text">Início:</b> {{$flow->start}}</p>
                <p><b class="red-text">Término:</b> {{$flow->finish}}</p>

                <p class="center"><b class="red-text">Tarefas:</b></p>
                @foreach ($flow->tasks as $task)
                <p class="green-text"> <i class="material-icons red-text">
                        check
                    </i>{{$task->name}}</p>
                @endforeach

                <div class="row center">
                    <a class="btn-flat tooltipped" onclick="editFlow({{$flow}})" data-position='left' data-delay='50' data-tooltip="Editar Flow">
                        <i class="material-icons blue-text">
                            edit
                        </i>
                    </a>
                    <a class="btn-flat tooltipped" onclick="askDelete({{$flow->id}})" data-position='bottom' data-delay='50' data-tooltip="Deletar Flow">
                        <i class="material-icons red-text">
                            clear
                        </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

<div class="fixed-action-btn">
    <a class="btn-floating btn-large red  btn tooltipped pulse" data-background-color="red lighten-3" data-position="left" data-delay="50" data-tooltip="Criar Flow" onclick="openModal()">
        <i class="large material-icons">add</i>
    </a>
</div>


<!-- Modal Structure -->
<div id="modalDelete" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 class="center red-text">Deletar Flow?</h4>
        <div class="row center">
            <input type="hidden" id="deleteInput">
            <a class="btn-flat tooltipped" onclick="deleteUser()" data-position='left' data-delay='50' data-tooltip="Sim">
                <i class="material-icons blue-text">
                    done
                </i>
            </a>
            <a class="btn-flat tooltipped" onclick="closeModal()" data-position='right' data-delay='50' data-tooltip="Não">
                <i class="material-icons red-text">
                    close
                </i>
            </a>
        </div>
    </div>
</div>


<!-- Modal Structure -->
<div id="modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 id="user" class="center red-text">Novo Flow</h4>
        <form class="col s12" method="POST" action="{{ URL::route('flows') }}" id="formFlow">
            <div class="row">
                <div class="col s3">
                    <label for="room">Quarto</label>
                    <input id="room" name="room" type="text" class="validate">
                </div>

                @if( Auth::user()->type->is_admin == 1 )
                <div class="input-field col s9">
                    <select name="user_id" id="user_id" required>
                        <option value="" disabled selected>Selecione</option>
                        @foreach ($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                    <label>Usuário</label>
                </div>
                @else
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                @endif
            </div>

            <div class="row">
                <div class="col s6">
                    <label for="start">Início</label>
                    <input id="start" name="start" type="datetime-local" class="validate">
                </div>

                <div class="col s6">
                    <label for="finish">Término</label>
                    <input id="finish" name="finish" type="datetime-local" class="validate">
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <select name="tasks[]" id="tasks" multiple>
                        <option value="" disabled selected>Selecione</option>
                        @foreach ($tasks as $task)
                        <option value="{{$task->id}}" {{ ($task->required ? ' selected' : "") }}>{{$task->name}}</option>
                        @endforeach
                    </select>
                    <label>Tarefas</label>
                </div>
            </div>
    </div>

    <div class="modal-footer">
        <button class="modal-action waves-effect waves-green btn-flat " type="submit">Salvar</button>
        </form>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Fechar</a>
    </div>
</div>
</div>
@if( method_exists($flows,'links') )
<h1 class="center">{{$flows->links('vendor.pagination.materializecss')}}</h1>
@endif

@endsection
<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/materialize.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('select').material_select();
        $("#start").val("<?= $start ?>");
        $("#finish").val("<?= $finish ?>");

    });

    function openModal() {
        this.clean();
        $('#modal').modal('open');
    }

    function closeModal() {
        this.clean();
        $('#modalDelete').modal('close');
    }

    function clean() {
        $('#formFlow').get(0).setAttribute('method', 'POST');
        $("#idFlow").remove();
        $("#nameUser").val('');
        $("#emailUser").val('');
        $("#passwordUser").val('');
        $("#passwordUser").attr('disabled', false);
        $("#required").prop('checked', false);
    }

    function editFlow(flow) {
        $("#idFlow").append(flow['id']);
        $("#user").html("Editar Flow");
        $("#room").val(flow['room']);
        $("#user_id").val(flow['user_id']);
        $("#user_id").material_select();
        $("#start").val(flow['start_input']);
        $("#finish").val(flow['finish_input']);

        $('<input>').attr({
            type: 'hidden',
            flow_id: 'idFlow',
            name: 'id',
            value: flow['id']
        }).appendTo('#formFlow');
        M.updateTextFields()
        $('#modal').modal('open');
    }

    function askDelete(id) {
        $('#modalDelete').modal('open');
        $("#deleteInput").val(id);
    }

    function deleteUser() {
        let id = $("#deleteInput").val();
        $.ajax({
            type: 'DELETE',
            url: 'flows',
            data: {
                "id": id
            },
            success: function($data) {
                $("#" + id).remove();
                $('.grid').masonry('reloadItems');;
                $('.grid').masonry({
                    itemSelector: '.grid-item',
                    columnWidth: 50
                });
                M.toast({
                    html: $data
                }, 5000);
                $("#modalDelete").modal("close");
                $("#deleteInput").val('');
            }
        });
    }
</script>