@extends('layouts.app')

@section('content')

<div class="container">
    <h4 class="green-text center">Rank </h4>
    <form method="POST" action="{{ URL::route('reports') }}">
        <div class="row">
            <div class="input-field col s10">
                <select name="month" id="month" required>
                    <option value="1">JAN</option>
                    <option value="2">FEV</option>
                    <option value="3">MAR</option>
                    <option value="4">ABR</option>
                    <option value="5">MAIO</option>
                    <option value="6">JUN</option>
                    <option value="7">JUL</option>
                    <option value="8">AGO</option>
                    <option value="9">SET</option>
                    <option value="10">OUT</option>
                    <option value="11">NOV</option>
                    <option value="12">DEZ</option>
                </select>
                <label>Mês</label>
            </div>
            <div class="input-field col s2">
                <button class=" btn" type="submit"> <i class="material-icons right">send</i>
                </button>
            </div>
    </form>
</div>

@if(count(json_decode($users)) > 1)
<div id="top_x_div" class="chart row"></div>
@else
<h3 class="center">Não há flows cadastrados nesse mês</h3>
@endif
</div>


@endsection
<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/materialize.min.js') }}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
    $(document).ready(function() {
        $("#month").val(<?= $month ?>);
    });


    google.charts.load('current', {
        'packages': ['bar']
    });
    google.charts.setOnLoadCallback(drawStuff);

    function drawStuff() {
        $user = <?= $users ?>;
        var data = new google.visualization.arrayToDataTable($user);

        var options = {
            title: '',
            legend: {
                position: 'none'
            },
            chart: {
                title: 'Quantidade de pontos por período dos(as) faxineiros(as)',
                subtitle: 'calculo = peso das tarefas * quant. de tarefas'
            },
            chartArea: {
                backgroundColor: "red",
            },
            bars: 'horizontal', // Required for Material Bar Charts.
            axes: {
                x: {
                    0: {
                        side: 'top',
                        label: 'Pontuação'
                    } // Top x-axis.
                }
            },
            bar: {
                groupWidth: "90%"
            }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        chart.draw(data, options);
    };
</script>