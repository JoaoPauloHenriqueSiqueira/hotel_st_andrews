@extends('layouts.app')

@section('content')

@if(count($users) == 0)
<h3 class="center">Não há usuários cadastrados</h3>
@endif

@foreach ($users as $user)
<div id="{{$user->id}}" class="grid-item z-depth-3">
    <div class="placeholder">
        <div class="gallery-curve-wrapper">
            <div class="gallery-header">
                <p><b class="red-text">Usuário:</b> {{$user->name}}</p>
                <p><b class="red-text">Email:</b> {{$user->email}}</p>
                <p><b class="red-text">Tipo:</b> {{$user->type->title}}</p>
                <div class="row center">
                    <a class="btn-flat tooltipped" onclick="editUser({{$user}})" data-position='left' data-delay='50' data-tooltip="Editar Usuário" {{ ($user->id ==  Auth::user()->id) ? 'disabled' : "" }}>
                        <i class="material-icons blue-text">
                            edit
                        </i>
                    </a>
                    <a class="btn-flat tooltipped" onclick="askDelete({{$user->id}})" data-position='bottom' data-delay='50' data-tooltip="Deletar Usuário" {{ ($user->id ==  Auth::user()->id) ? 'disabled' : "" }}>
                        <i class="material-icons red-text">
                            clear
                        </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

<div class="fixed-action-btn">
    <a class="btn-floating btn-large red  btn tooltipped pulse" data-background-color="red lighten-3" data-position="left" data-delay="50" data-tooltip="Criar Usuário" onclick="openModal()">
        <i class="large material-icons">add</i>
    </a>
</div>


<!-- Modal Structure -->
<div id="modalDelete" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 class="center red-text">Deletar Usuário?</h4>
        <div class="row center">
            <input type="hidden" id="deleteInput">
            <a class="btn-flat tooltipped" onclick="deleteUser()" data-position='left' data-delay='50' data-tooltip="Sim">
                <i class="material-icons blue-text">
                    done
                </i>
            </a>
            <a class="btn-flat tooltipped" onclick="closeModal()" data-position='right' data-delay='50' data-tooltip="Não">
                <i class="material-icons red-text">
                    close
                </i>
            </a>
        </div>
    </div>
</div>


<!-- Modal Structure -->
<div id="modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 id="user" class="center red-text">Novo Usuário</h4>
        <form class="col s12" method="POST" action="{{ URL::route('users') }}" id="formUser">
            <div class="row">
                <div class="input-field col s12">
                    <input id="nameUser" name="name" type="text" class="validate">
                    <label for="disabled">Nome</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input id="emailUser" name="email" type="text" class="validate">
                    <label for="disabled">Email</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input id="passwordUser" name="password" type="text" class="validate">
                    <label for="disabled">Senha</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <select name="type_id" id="typeUser" required>
                        <option value="1">Gerente</option>
                        <option value="2">Faxineira</option>
                    </select>
                    <label>Tipo</label>
                </div>
            </div>
    </div>

    <div class="modal-footer">
        <button class="modal-action waves-effect waves-green btn-flat " type="submit">Salvar</button>
        </form>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Fechar</a>
    </div>
</div>

</div>
@if( method_exists($users,'links') )
<h1 class="center">{{$users->links('vendor.pagination.materializecss')}}</h1>
@endif

@endsection
<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/materialize.min.js') }}"></script>

<script>
    function openModal() {
        this.clean();
        $('#modal').modal('open');
    }

    function closeModal() {
        this.clean();
        $('#modalDelete').modal('close');
    }

    function clean() {
        $('#formUser').get(0).setAttribute('method', 'POST');
        $("#put").remove();
        $("#idUser").remove();
        $("#nameUser").val('');
        $("#emailUser").val('');
        $("#passwordUser").val('');
        $("#passwordUser").attr('disabled', false);
        $("#required").prop('checked', false);
    }

    function editUser(user) {
        $("#idUser").append(user['id']);
        $("#user").html("Editar Usuário");
        $("#nameUser").val(user['name']);
        $("#emailUser").val(user['email']);
        $("#passwordUser").attr('disabled', true);
        $("#typeUser").val(user['type_id']);
        $("#typeUser").formSelect();
        $('<input>').attr({
            type: 'hidden',
            user_id: 'idUser',
            name: 'id',
            value: user['id']
        }).appendTo('#formUser');
        $('<input>').attr({
            id: 'put',
            type: 'hidden',
            name: '_method',
            value: 'PUT'
        }).appendTo('#formUser');
        M.updateTextFields()
        $('#modal').modal('open');
    }

    function askDelete(id) {
        $('#modalDelete').modal('open');
        $("#deleteInput").val(id);
    }

    function deleteUser() {
        let id = $("#deleteInput").val();
        $.ajax({
            type: 'DELETE',
            url: 'users',
            data: {
                "id": id
            },
            success: function($data) {
                $("#" + id).remove();
                $('.grid').masonry('reloadItems');;
                $('.grid').masonry({
                    itemSelector: '.grid-item',
                    columnWidth: 50
                });
                M.toast({
                    html: $data
                }, 5000);
                $("#modalDelete").modal("close");
                $("#deleteInput").val('');
            }
        });
    }
</script>