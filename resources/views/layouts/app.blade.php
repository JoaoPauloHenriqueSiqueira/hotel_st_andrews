  <html lang="pt-br">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="https://www.revelare.com.br/assets/favicon/favicon.ico">
    <title>Flows</title>

    <!-- Lato Font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/gallery-materialize.min.opt.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
      .nav-extended {
        background-image: url(https://ak.picdn.net/shutterstock/videos/21658243/thumb/1.jpg);
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
      }


      .grid-item {
        width: 290px;
        margin: 5px;
      }

      .modal-content {
        height: 100% !important;
      }

      .material-tooltip {
        background-color: purple;
      }

      .caret {
        display: none;
      }

      .modal-flow {
        height: 55% !important;
      }

      #modalDelete {
        height: 25% !important;
      }

      .chart {
        width: 100%;
        min-height: 450px;
      }

      .row {
        margin: 0 !important;
      }

      .legend {
        margin: 20px;
      }

      body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
      }

      main {
        flex: 1 0 auto;
      }
    </style>

  </head>


  <body class="vsc-initialized">
    <main>
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <script>
            M.toast({
              html: '{{$error}}'
            }, 5000);
          </script>
          @endforeach
        </ul>
      </div>
      @endif

      @if(session()->has('message'))
      <div class="alert alert-success">
        <script>
          M.toast({
            html: '{{ session()->get("message")}}'
          }, 5000);
        </script>
      </div>
      @endif
      <!-- Navbar and Header -->
      <nav class="nav-extended nav  blue">
        <div class="nav-background">
          <div class="ea k">

          </div>
        </div>
        <div class="nav-wrapper db">
          <ul class="bt">
            <li>
              <a class="white-text" href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>

          </ul>
          <!-- Dropdown Structure -->

          <div class="nav-header de">
            <img src="{{url('/img/logo.png')}} " width="50" class="center" />
          </div>
      </nav>
      <ul class="side-nav" id="nav-mobile" style="transform: translateX(-100%);">
      </ul>

      <nav class="nav-dark green lighteen-1">
        <div class="nav-wrapper">
          <a class="brand-logo right hide-on-med-and-down">
            <span class="material-icons text-white">
              landscape
            </span>
            Hotel St. Andrews</a>
          <ul>
            @if( Auth::user()->type->is_admin == 1 )
            <li id="home">
              <a class="tooltipped" href="{{ URL::route('home') }}" data-position='right' data-delay='50' data-tooltip="Início">Início</a>
            </li>
            <li id="reports">
              <a class="tooltipped" href="{{ URL::route('reports') }}" data-position='right' data-delay='50' data-tooltip="Relatório">Gráficos</a>
            </li>
            <li id="tasks">
              <a class="tooltipped" href="{{ URL::route('tasks') }}" data-position='right' data-delay='50' data-tooltip="CRUD para as TAREFAS">Tarefas</a>
            </li>
            <li id="users">
              <a class="tooltipped" href="{{ URL::route('users') }}" data-position='right' data-delay='50' data-tooltip="CRUD para as USUÁRIOS">Usuários</a>
            </li>

            @endif
            <li id="flows">
              <a class="tooltipped" href="{{ URL::route('flows') }}" data-position='right' data-delay='50' data-tooltip="CRUD para as FLOWS">Flows</a>
            </li>
          </ul>
        </div>
      </nav>

      <div id="portfolio" class="cx gray">
        <div class="db">
          <div class="b e messages grid">
            @yield('content')
          </div>
        </div>
      </div><!-- /.container -->
    </main>

  </body>
  <!-- Core Javascript -->
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.2/js/materialize.min.js"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

  <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>

  <script>
    $(document).ready(function() {
      $('.modal1').modal();
      $('select').material_select();
      $('#modalDelete').modal();
      document.querySelectorAll('.select-wrapper').forEach(t => t.addEventListener('click', e => e.stopPropagation()))
    });

    $("#{{\Request::route()->getName()}}").addClass("active");

    $('.grid').masonry({
      // options
      itemSelector: '.grid-item',
      columnWidth: 50
    });

    $('#modal').modal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
    });
  </script>

  </html>