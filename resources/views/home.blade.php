@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row col 12">
        <div class="col s12 m4">
            <div class="card purple darken-1">
                <div class="card-content white-text">
                    <span class="card-title center">Gerentes</span>
                    <h3 class="center">
                        {{$metrics['admins']}}
                    </h3>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card orange darken-1">
                <div class="card-content white-text">
                    <span class="card-title center">Faxineiros</span>
                    <h3 class="center">
                        {{$metrics['notAdmins']}}
                    </h3>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card red darken-1">
                <div class="card-content white-text">
                    <span class="card-title center">Flows do dia</span>
                    <h3 class="center">
                        {{$metrics['flows']}}
                    </h3>
                </div>
            </div>
        </div>
    </div>

    <h4 class="green-text center">Rank </h4>
    <form method="POST" action="{{ URL::route('home') }}">
        <div class="row">
            <div class="input-field col s10">
                <select name="month" id="month" required>
                    <option value="1">JAN</option>
                    <option value="2">FEV</option>
                    <option value="3">MAR</option>
                    <option value="4">ABR</option>
                    <option value="5">MAIO</option>
                    <option value="6">JUN</option>
                    <option value="7">JUL</option>
                    <option value="8">AGO</option>
                    <option value="9">SET</option>
                    <option value="10">OUT</option>
                    <option value="11">NOV</option>
                    <option value="12">DEZ</option>
                </select>
                <label>Mês</label>
            </div>
            <div class="input-field col s2">
                <button class=" btn" type="submit"> <i class="material-icons right">send</i>
                </button>
            </div>
    </form>
</div>


@if(count(json_decode($users)) == 0)
<h3 class="center">Não há flows cadastrados nesse mês</h3>
@endif

<ul class="collapsible popout" data-collapsible="accordion">
    @foreach ($users as $k => $v)
    <li>
        <div class="collapsible-header  {{ ($k == 0 ? 'active' : "" )}}">
            <span class="center">
                <a class="btn-floating green centerbtn-small pulse center">{{$k+1 }}</a>
                <span class="legend">{{$v->name}} - <span class="red-text">Pontos:</span> {{$v->points}}</span>
            </span>
        </div>
        <div class="collapsible-body">
            <p>
                <span class="red-text">Tarefas:</span> {{$v->tasks}} <br>
                <span class="red-text">Total trab:</span> {{$v->duration}} Minutos<br>
                <span class="red-text">+ rápida:</span> {{$v->fast}} <span class="red-text">Nome: </span> {{$v->fast_name}} <br>
                <span class="red-text">- rápida:</span> {{$v->slow}} <span class="red-text">Nome: </span>{{$v->slow_name}}
            </p>
        </div>
    </li>
    @endforeach
</ul>
</div>


@endsection
<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/materialize.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('.collapsible').collapsible();
        $("#month").val(<?= $month ?>);
    });
</script>