<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlowsTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flows_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('duration')->unsigned();
            $table->integer('flow_id')->unsigned();
            $table->integer('task_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('flows_tasks', function ($table) {
            $table->foreign('flow_id')->references('id')->on('flows')->onDelete('cascade');
        });

        Schema::table('flows_tasks', function ($table) {
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flows_tasks');
    }
}
