<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "id" => 1,
                'name' => "Erick Pacheli",
                'email' => 'erick@gmail.com',
                'type_id' => 1,
                'password' => bcrypt('master123'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                "id" => 2,
                'name' => "Leonardo da Silva",
                'email' => 'leonardo@gmail.com',
                'type_id' => 2,
                'password' => bcrypt('leonardo123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                "id" => 3,
                'name' => "Janaína Côrrea",
                'email' => 'janaina@gmail.com',
                'type_id' => 2,
                'password' => bcrypt('janaina321'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                "id" => 4,
                'name' => "Paulo Lungatto",
                'email' => 'paulo@gmail.com',
                'type_id' => 1,
                'password' => bcrypt('123paulo123'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                "id" => 5,
                'name' => "Julia Maria",
                'email' => 'juliamaria@gmail.com',
                'type_id' => 1,
                'password' => bcrypt('julia'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                "id" => 6,
                'name' => "Rodolfo Silveira",
                'email' => 'rodolfosilveira@gmail.com',
                'type_id' => 1,
                'password' => bcrypt('rasjdnsoadn'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        foreach ($data as $row) {
            $user = DB::table('users')->find($row['id']);

            if (!$user) {
                DB::table('users')->insert($row);
            }
        }
    }
}
