<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "id" => 1,
                'title' => "Gerente",
                'is_admin' => 1
            ],
            [
                "id" => 2,
                'title' => "Faxineira",
                'is_admin' => 0
            ]
        ];

        foreach ($data as $row) {
            $type = DB::table('types')->find($row['id']);

            if (!$type) {
                DB::table('types')->insert($row);
            }
        }
    }
}
