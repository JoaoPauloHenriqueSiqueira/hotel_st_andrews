<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "id" => 1,
                "name" => "Arrumar a cama",
                "required" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 2,
                "name" => "Retirar o lixo",
                "required" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 3,
                "name" => "Limpar o chão",
                "required" => 0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 4,
                "name" => "Limpar a parede",
                "required" => 0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 5,
                "name" => "Arrumar as cadeiras",
                "required" => 0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 6,
                "name" => "Lavar a louça",
                "required" => 0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 7,
                "name" => "Reabastecer produtos de higiene pessoal",
                "required" => 0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        ];
 
        foreach ($data as $row) {
            $tax = DB::table('tasks')->find($row['id']);

            if (!$tax) {
                DB::table('tasks')->insert($row);
            }
        }
    }
}
