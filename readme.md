# Hotel St. Andrews - Workflow

Projeto para teste dev - PHP

## Requisitos

O projeto foi aplicado em um sistema Linux - Ubuntu (19.10)

```bash
- docker
- git
```
## Criação das pastas

Descompactar conteúdo docker (anexado no email) em uma pasta no sistema.

Na pasta em que descompactamos os arquivos docker, iremos clonar o projeto:

```json
git clone git@gitlab.com:JoaoPauloHenriqueSiqueira/hotel_st_andrews.git revelare
```

## Containers
Há 3 containers: PHP, Mysql e Phpmyadmin, subi-los através do comando

```json
sudo docker-compose up -d
```

## Database

Neste momento, já devemos ser capazes de acessar o phpmyadmin através da url: http://localhost:3380/index.php e criar o banch revelare

## Configs .env
Copiar o .env.example 

```json
cp .env.example .env
```

Alterar conteúdo novo arquivo:

```json
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=revelare
DB_USERNAME=root
DB_PASSWORD=root   
``` 

## Configurar aplicação

Acessar o terminal  do container php:

```json
sudo docker exec -it php-dev bash 
```
Ou 

```json
sudo docker exec -it 9008019917e3 bash
```

*9008019917e3* é o ID do meu container, é possível listar os containers através do comando

```json
 sudo docker ps
```
Ir até a pasta do projeto no container:

```json
cd revelare
```

Instalar composer dentro do repositório php

```json
curl -s https://getcomposer.org/installer | php
```

Talvez seja necessário gerar token no git
```json
apt-get update
install git-all
```

E instalar dependências

```json
./composer.phar install

```

Iremos também dar permissão para o diretório de logs:

```json
chmod -R 777 storage/
``` 

E finalmente criar key para aplicação:

```json
php artisan key:generate

```


## Popular DB e c

Ainda dentro do container PHP, iremos executar os comandos:

Criar tabelas:
```json
php artisan migrate

```

Popular tabelas:

```json
php artisan db:seed
```

## Acessando sistema

O endereço para acessar é http://localhost/revelare/public/login e um dos usuários gerados no seed, são:

```json
Usuário: erick@gmail.com
Senha: master123
```

