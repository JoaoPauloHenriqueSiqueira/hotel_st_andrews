<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Flows extends Model
{
    protected $collection = 'flows';
    protected $fillable = ['start', 'finish', 'room', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
        return $this->belongsToMany(Tasks::class, 'flows_tasks', 'flow_id', 'task_id')
            ->withPivot('id')
            ->withPivot('duration')
            ->withPivot('flow_id')
            ->withPivot('task_id');;
    }

    public function points()
    {
        return $this->belongsToMany(User::class, 'flow_user_points', 'flow_id', 'flow_id')
        ->withPivot('points')
        ->withPivot('created_at')
        ->withPivot('updated_at');
    }

    /**
     * Mutator para data
     *
     * @param [type] $date
     * @return string
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i');
    }

    public function getStartAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i');
    }

    public function getFinishAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i');
    }
}
