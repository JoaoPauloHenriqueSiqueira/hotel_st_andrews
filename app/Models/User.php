<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function points()
    {
        return $this->belongsToMany(Flows::class, 'flow_user_points', 'user_id', 'flow_id')
            ->withPivot('points')
            ->withPivot('created_at')
            ->withPivot('updated_at');
    }

    public function type()
    {
        return $this->belongsTo(Types::class);
    }
    
}
