<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Tasks extends Model
{
    protected $collection = 'tasks';
    protected $fillable = ['name', 'type_id', 'required'];

    public function type()
    {
        return $this->belongsTo(Types::class);
    }

    public function flows()
    {
        return $this->belongsToMany(Flows::class, 'flows_tasks');
    }

    /**
     * Mutator para data
     *
     * @param [type] $date
     * @return string
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i');
    }
}
