<?php

namespace App\Services;

use App\Repositories\Contracts\UserRepositoryInterface;

class UserService
{
    protected $repository;

    /**
     * Cria instancia de Serviço
     *
     * @return void
     */
    public function __construct(
        UserRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * Pagina Usuários
     *
     * @return void
     */
    public function get()
    {
        return $this->repository->orderBy('name')->paginate(6);
    }

    /**
     * All repository
     */
    public function allNotAdmin()
    {
        return $this->repository->allNotAdmin();
    }

    public function allAdmin()
    {
        return $this->repository->allAdmin();
    }

    public function ranking($month)
    {
        return $this->repository->ranking($month);
    }

    public function graphRanking($month)
    {
        $users = $this->repository->graphRanking($month);

        $usersList = [['Faxineiros(as)', 'Pontos']];

        foreach ($users as $user) {
            $usersList[] = [$user->name, $user->points];
        }

        return json_encode($usersList);
    }

    

    /**
     * Procura por usuário
     *
     * @param [type] $UserId
     * @return void
     */
    public function find($userId)
    {
        return $this->repository->find($userId)->toArray();
    }

    /**
     * Salva/atualiza registro no banco
     *
     * @param [type] $request
     * @return void
     */
    public function save($request)
    {
        if ($request->validated()) {
            $request['password'] = bcrypt(array_get($request, "password"));

            $update = array_get($request, "id", false);
            if ($update) {
                $request = $this->verifyUpdate($request, $this->find($update));
            }

            $response = $this->repository->updateOrCreate(["id" => array_get($request, "id")], $request->all());
            if ($response) {
                return redirect()->back()->with('message', 'Registro criado/atualizado!');
            }
        }

        return redirect()->back()->with('message', 'Ocorreu algum erro');
    }

    /**
     * Remove password, caso atualização
     *
     * @param [type] $request
     * @return void
     */
    private function verifyUpdate($request)
    {
        unset($request['password']);
        return $request;
    }

    /**
     * Deleta usuário
     *
     * @param [type] $request
     * @return void
     */
    public function delete($request)
    {
        $userId = array_get($request, "id");
        $response = $this->repository->delete($userId);

        if ($response) {
            return response('Removido com sucesso', 200);
        }

        return response('Ocorreu algum erro ao remover', 422);
    }
}
