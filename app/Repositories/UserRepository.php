<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function model()
    {
        return User::class;
    }

    /**
     * Pega todos os administradores
     *
     * @return void
     */
    public function allAdmin()
    {
        return DB::table('users')
            ->join('types', 'types.id', '=', 'users.type_id')
            ->select('users.*')
            ->orderBy("name")
            ->where("types.is_admin", true)
            ->get();
    }

    /**
     * Pega todos os não administradores
     *
     * @return void
     */
    public function allNotAdmin()
    {
        return DB::table('users')
            ->join('types', 'types.id', '=', 'users.type_id')
            ->select('users.*')
            ->orderBy("name")
            ->where("types.is_admin", false)
            ->get();
    }

    /**
     * Rankeia usuários com mais atividades em menor tempo
     * Consulta tbm tarefas realizadas por eles, 
     * mais rápidas e mais lentas
     *
     * @param [type] $month
     * @return void
     */
    public function ranking($month)
    {
        $date = Carbon::createFromFormat('m', $month);
        $start = $date->copy()->startOfMonth();
        $finish = $date->copy()->endOfMonth();

        $subQuery = DB::table('users')
            ->join('flow_user_points', 'flow_user_points.user_id', '=', 'users.id')
            ->join('flows_tasks', 'flow_user_points.flow_id', '=', 'flows_tasks.flow_id')
            ->join('flows', 'flow_user_points.flow_id', '=', 'flows.id')
            ->select(
                'users.*',
                DB::raw('SUM(distinct flow_user_points.points) as points'),
                DB::raw('SUM(flows_tasks.duration) as duration'),
                DB::raw('COUNT(flows_tasks.ID) as tasks'),
                DB::raw('MIN(flows_tasks.duration) as fast'),
                DB::raw('MAX(flows_tasks.duration) as slow'),
            )
            ->groupBy("users.id")
            ->orderBy("points", 'DESC')
            ->where('flows.start', '>=', $start)
            ->where('flows.finish', '<=', $finish);


        $query = DB::table(DB::raw('(' . $subQuery->toSql() . ') as o1'))
            ->join('flows_tasks as o2', 'o2.duration', '=', 'o1.fast')
            ->join('tasks as o3', 'o2.task_id', '=', 'o3.id')
            ->join('flows_tasks as o4', 'o4.duration', '=', 'o1.slow')
            ->join('tasks as o5', 'o4.task_id', '=', 'o5.id')
            ->select(
                '*',
                'o3.name as fast_name',
                'o5.name as slow_name',
                'o1.name as name'
            )
            ->groupBy("o1.id")
            ->orderBy("points", 'DESC')
            ->mergeBindings($subQuery);
            
        return $query->get();
    }

    public function graphRanking($month)
    {
        $date = Carbon::createFromFormat('m', $month);
        $start = $date->copy()->startOfMonth();
        $finish = $date->copy()->endOfMonth();

        return DB::table('users')
            ->join('flow_user_points', 'flow_user_points.user_id', '=', 'users.id')
            ->join('flows', 'flow_user_points.flow_id', '=', 'flows.id')
            ->select(
                'users.name',
                DB::raw('SUM(distinct flow_user_points.points) as points'),
            )
            ->orderBy("points", 'DESC')
            ->groupBy("users.id")
            ->where('flows.start', '>=', $start)
            ->where('flows.finish', '<=', $finish)
            ->get();
    }
}
