<?php

namespace App\Repositories;

use App\Models\Tasks;
use App\Repositories\Contracts\TaskRepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;

class TaskRepository extends BaseRepository implements TaskRepositoryInterface
{
    public function model()
    {
        return Tasks::class;
    }
}
