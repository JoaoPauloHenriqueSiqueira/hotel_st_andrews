<?php

namespace App\Role;

use App\Models\User;

class AdminCheck
{
    public function check($user)
    {
        if ($user) {
            if ($user->type->isAdmin()) {
                return true;
            }
        }
        return false;
    }
}
