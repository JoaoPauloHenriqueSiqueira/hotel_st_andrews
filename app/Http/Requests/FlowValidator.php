<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class FlowValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room' => 'required',
            'user_id' => 'required|exists:users,id',
            'start' => 'required|before:finish',
            'finish' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'room_id.required' => 'Número do quarto é um campo necessário',
            'user_id.required' => 'Usuário é um campo necessário',
            'start.required' => 'Data de início é um campo necessário',
            'start.before' => 'Data de início precisa ser menor que data de término',
            'finish.required' => 'Data de término é um campo necessário'
        ];
    }
}
