<?php

namespace App\Http\Controllers;

use App\Http\Requests\FlowValidator;
use App\Http\Requests\TaskPost;
use App\Services\FlowService;
use App\Services\TaskService;
use App\Services\UserService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class FlowsController extends Controller
{
    protected $service;
    protected $userService;
    protected $taskService;

    /**
     * Função construtora
     *
     * @param FlowService $service
     */
    public function __construct(
        FlowService $service,
        UserService $userService,
        TaskService $taskService
    ) {
        $this->service = $service;
        $this->userService = $userService;
        $this->taskService = $taskService;
    }

    /**
     * Renderiza view com tasks
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        try {          
            return view('flows', [
                "flows" => $this->service->get(),
                "tasks" => $this->taskService->all(),
                "users" => $this->userService->allNotAdmin(),
                "start" => Carbon::now()->format('Y-m-d\TH:i'),
                "finish" => Carbon::now()->addHour(1)->format('Y-m-d\TH:i')
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Cria ou atualiza dados no banco
     *
     * @param TaskPost $request
     * @return void
     */
    public function createOrUpdate(FlowValidator $request)
    {
        try {
            return $this->service->save($request);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Deleta dado do banco
     *
     * @param Request $request
     * @return void
     */
    public function delete(Request $request)
    {
        try {
            return $this->service->delete($request);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
