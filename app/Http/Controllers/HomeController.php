<?php

namespace App\Http\Controllers;


use App\Services\FlowService;
use App\Services\TaskService;
use App\Services\UserService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $service;
    protected $userService;
    protected $taskService;

    /**
     * Construct function
     *
     * @param FlowService $service
     */
    public function __construct(
        FlowService $service,
        UserService $userService,
        TaskService $taskService
    ) {
        $this->service = $service;
        $this->userService = $userService;
        $this->taskService = $taskService;
    }

    /**
     * Renderiza view com tasks
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        try {
            $metrics = [];
            $metrics['admins'] = $this->userService->allAdmin()->count();
            $metrics['notAdmins'] = $this->userService->allNotAdmin()->count();
            $metrics['flows'] = $this->service->flowToday()->count();

            $month = array_get($request, "month", Carbon::now()->format('m'));
            return view('home', [
                "users" => $this->userService->ranking($month),
                "metrics" => $metrics,
                "month" => $month
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Renderiza gráfico de ranking
     *
     * @param Request $request
     * @return void
     */
    public function reports(Request $request)
    {
        try {
            $month = array_get($request, "month", Carbon::now()->format('m'));
            $users = $this->userService->graphRanking($month);

            return view('report', [
                "users" => $users,
                "month" => $month
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
